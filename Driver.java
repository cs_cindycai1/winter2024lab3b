import java.util.Scanner;
import java.util.Random;

/*create a herd of rabbit
*enter their stats
*choose what action they want to take when they ancounter another animal
*/
public class Driver {
	public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("You are a herd of rabbits living in a forest.");
		
		//create array of rabbit and enter field for each value
		Rabbit[] rabbitHerd = new Rabbit[4];
		for (int i = 0; i < rabbitHerd.length; i++) {
			rabbitHerd[i] = new Rabbit();
			
			System.out.println("Enter the color of rabbit #" + (i + 1));
			rabbitHerd[i].color = reader.nextLine();
			
			System.out.println("Enter the defense of rabbit #" + (i + 1) + " (on 10)");
			rabbitHerd[i].defense = Integer.parseInt(reader.nextLine());
			
			System.out.println("Enter the power of rabbit #" + (i + 1) + " (on 10)");
			rabbitHerd[i].power = Integer.parseInt(reader.nextLine());
			
			System.out.println("");
		}
		
		startEvent(rabbitHerd);
	}
	
	public static void startEvent(Rabbit[] rabbitHerd) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		Random rand = new Random();
		
		int rabbitNumber = rand.nextInt(rabbitHerd.length);
		
		System.out.println("Rabbit #" + (rabbitNumber + 1) + " encounters another animal");
		System.out.println("You choose to: ");
		System.out.println("1: fight");
		System.out.println("2: enter a beauty contest");
		
		
		//choose whether they want to fight or to enter a beauty contest
		int choice = Integer.parseInt(reader.nextLine());
		if (choice == 1) {
			rabbitHerd[rabbitNumber].battle();
		} else if (choice == 2) {
			rabbitHerd[rabbitNumber].compareColor();
		}
	}
}