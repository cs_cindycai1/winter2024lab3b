public class Rabbit {
	//field
	public int defense;
	public int power;
	public String color;
	
	/*compare power and display who is stronger
	*if power is equal, compare defense and conclude result
	*/
	public void battle() {
		int[] opponent = new int[2];
		opponent[0] = 6; //opponent power
		opponent[1] = 7; //opponent defense
		
		System.out.println("You've chosen to fight!");
		System.out.println("The result is---");
		
		System.out.println("");
		
		if (this.power < opponent[0]) {
			System.out.println("You lost...");
		} else if (this.power > opponent[0]) {
			System.out.println("You won!");
		} else {
			if (this.defense == opponent[1]) {
				System.out.println("You are equal in strength");
			} else if (this.defense < opponent[1]) {
				System.out.println("You lost...");
			} else {
				System.out.println("You won!");
			}
		}
	}
	
	/*
	*compare object color and print result
	*/
	public void compareColor() {
		System.out.println("You've chosen to enter a beauty contest together!");
		System.out.println("The result is---");
		
		if (this.color.equals("red") || this.color.equals("blue")) {
			System.out.println("You won first place!");
		} else if (this.color.equals("black") || this.color.equals("white")) {
			System.out.println("You lost against your opponent...");
		} else {
			System.out.println("You won against your opponent...but you both are in the bottom half of the ranking");
		}
	}
}